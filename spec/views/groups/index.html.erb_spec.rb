require 'rails_helper'

RSpec.describe "groups/index", type: :view do
  before(:each) do
    assign(:groups, [
      Group.create!(
        :name => "Name",
        :description => "MyText has to be 20 characters or more"
      ),
      Group.create!(
        :name => "Name",
        :description => "MyText has to be 20 characters or more"
      )
    ])
  end

  it "renders a list of groups" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText has to be 20 characters or more".to_s, :count => 2
  end
end
