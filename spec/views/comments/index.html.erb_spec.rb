require 'rails_helper'

RSpec.describe "comments/index", type: :view do
  before(:each) do
    assign(:comments, [
      Comment.create!(
        :user_id => 1,
        :challenge_id => 2,
        :comment => "MyText has to be 20 characters"
      ),
      Comment.create!(
        :user_id => 1,
        :challenge_id => 2,
        :comment => "MyText has to be 20 characters"
      )
    ])
  end

  it "renders a list of comments" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText has to be 20 characters".to_s, :count => 2
  end
end
