require 'rails_helper'

RSpec.describe "challenges/index", type: :view do
  before(:each) do
    assign(:challenges, [
      Challenge.create!(
        :name => "Name",
        :description => "This is a good description",
        :moderation_flag => false,
        :status => "Status",
        :terms => "MyText"
      ),
      Challenge.create!(
        :name => "Name",
        :description => "This is a good description",
        :moderation_flag => false,
        :status => "Status",
        :terms => "MyText"
      )
    ])
  end

  it "renders a list of challenges" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "This is a good description".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
