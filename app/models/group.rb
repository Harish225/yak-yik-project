class Group < ActiveRecord::Base

  has_many :accepts, as: :acceptor
  has_many :accepted, :through => :accepts, :source => :accepted, source_type: "Challenge"
  has_many :challenges, as: :creator
  has_many :contributed, :through => :contributions, :source => :contributed, source_type: "Challenge"
  has_many :contributions, as: :contributor
  has_many :users, through: :user_groups
  has_many :user_groups

  validates :name,            presence: :true,
                              length: { maximum: 45 }
  validates :description,     presence: :true,
                              length: { within: 20..250 }

end
