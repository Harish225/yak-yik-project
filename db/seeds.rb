# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# User.create!( :first_name => 'vamshi', :last_name => 'ns', :username => 'vamshins', :email => 'vamshi.krishna588@gmail.com', :password => 'password', :sign_in_count => 0, :created_at => '2015-10-13', :updated_at => '2015-10-13')
# Challenge.create!( :challenge_id => '1001', :name => 'First Challenge', :description => 'First Challenge in YakYik' )